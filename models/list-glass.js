class ListGlasses {
    constructor() {
        this.array = [];
    }
    _findIndex(id) {
        return this.array.findIndex((el) => el.id === id);
    }

}
export default ListGlasses;