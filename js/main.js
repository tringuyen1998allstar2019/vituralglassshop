import listGlass from "./../models/list-glass.js"
let dataGlasses = [
    { id: "G1", src: "./img/g1.jpg", virtualImg: "./img/v1.png", brand: "Armani Exchange", name: "Bamboo wood", color: "Brown", price: 150, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? " },
    { id: "G2", src: "./img/g2.jpg", virtualImg: "./img/v2.png", brand: "Arnette", name: "American flag", color: "American flag", price: 150, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G3", src: "./img/g3.jpg", virtualImg: "./img/v3.png", brand: "Burberry", name: "Belt of Hippolyte", color: "Blue", price: 100, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G4", src: "./img/g4.jpg", virtualImg: "./img/v4.png", brand: "Coarch", name: "Cretan Bull", color: "Red", price: 100, description: "In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G5", src: "./img/g5.jpg", virtualImg: "./img/v5.png", brand: "D&G", name: "JOYRIDE", color: "Gold", price: 180, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?" },
    { id: "G6", src: "./img/g6.jpg", virtualImg: "./img/v6.png", brand: "Polo", name: "NATTY ICE", color: "Blue, White", price: 120, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G7", src: "./img/g7.jpg", virtualImg: "./img/v7.png", brand: "Ralph", name: "TORTOISE", color: "Black, Yellow", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam." },
    { id: "G8", src: "./img/g8.jpg", virtualImg: "./img/v8.png", brand: "Polo", name: "NATTY ICE", color: "Red, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim." },
    { id: "G9", src: "./img/g9.jpg", virtualImg: "./img/v9.png", brand: "Coarch", name: "MIDNIGHT VIXEN REMIX", color: "Blue, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet." }
];
let listGlasses = new listGlass();
listGlasses = dataGlasses;
console.log(listGlasses);

const showListGlasses = (arr) => {
    let contentHTML = "";
    arr.forEach(glasses => {
        contentHTML += `
        <div class="col-3 mr-4 mb-4" onclick="wearGlass('${glasses.id}')">
        <div class="card" style="width: 8rem; height:8rem">
        <img src="${glasses.src}"/>
        </div>
        </div>
        `
    });
    document.getElementById("vglassesList").innerHTML = contentHTML;
}
const wearGlass = (id) => {
    let index = listGlasses.findIndex(glasses => glasses.id === id);
    let imgVitural =
        `
        <img src="${listGlasses[index].virtualImg}" id="glassVitur"/>
        `;
    let infoGlass = `
        <p>${listGlasses[index].name} - ${listGlasses[index].color}</p>
        <p class="btn btn-danger ">${"$" +listGlasses[index].price} </p><p class="text-success d-inline ml-2">Stocking</p>
        <p>${listGlasses[index].description}</p>
    `
    document.getElementById("glassesInfo").style.display = "block";
    document.getElementById("glassesInfo").innerHTML = infoGlass;
    document.getElementById("avatar").innerHTML = imgVitural;
}
const removeGlasses = (status) => {
    console.log(status);
    if (status) {
        console.log("heloo");
        document.getElementById("glassVitur").style.display = "none";
    } else {
        document.getElementById("glassVitur").style.display = "block";
    }

}
window.removeGlasses = removeGlasses;
window.wearGlass = wearGlass;

showListGlasses(listGlasses);